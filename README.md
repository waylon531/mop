# MOP
### A tool for performing matrix operations

Mop is a command-line tool that makes use of matrixlab
to perform operations on matricies via the command line.


## Usage


```
  mop gmres [options] <matrix> <vector>
  mop mul <matrix> <matrix2>
  mop mul (-v|--vec) <matrix> <vector>
  mop transpose <matrix>
  mop show <matrix>
  mop (-h | --help)
  mop --version
```

* `mop gmres` finds a solution to the matrix using the GMRES algorithm
* `mop mul` multiplies a matrix by either a vector or another matrix
* `mop transpose` performs a transpose on the matrix and then prints it out
* `mop show` just prints the matrix out

For more information type `mop -h`

## Examples

Multiply a matrix times itself
`mop mul bcspwr02.mtx bcspwr02.mtx`

Multiply a matrix times a vector
```sh
$ mop mul --vec bcspwr02.mtx $(echo -n "["; for i in `seq 49`; do echo -n "1,";done; echo "]")

[2.0, 4.0, 4.0, 2.0, 3.0, 3.0, 4.0, 3.0, 2.0, 2.0, 2.0, 2.0, 2.0, 6.0, 6.0, 4.0, 3.0, 4.0, 3.0, 4.0, 3.0, 5.0, 7.0, 2.0, 2.0, 2.0, 3.0, 6.0, 2.0, 3.0, 2.0, 6.0, 4.0, 3.0, 3.0, 3.0, 6.0, 4.0, 3.0, 2.0, 4.0, 4.0, 2.0, 2.0, 5.0, 5.0, 2.0, 2.0, 5.0]

```

Solve a matrix with gmres
```sh
$ mop gmres six.mtx '[1,1,1]'

GMRES Successful: [0.9999999999999998, 0.3333333333333333, 0.6666666666666665]

$ mop show six.mtx

[1,0,0]
[0,3,0]
[0,1,1]

```
