//
//    mop, a tool for performing matrix operations
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//    

#[macro_use]
extern crate serde_derive;
extern crate docopt;

#[macro_use] extern crate lalrpop_util;
lalrpop_mod!(pub matrix); // synthesized by LALRPOP


use docopt::Docopt;

use matrixlab::from_file;

use std::path::Path;


const USAGE: &'static str = "
mop

A command line tool for performing matrix operations

Usage:
  mop gmres [options] <matrix> <vector>
  mop mul <matrix> <matrix2>
  mop mul (-v|--vec) <matrix> <vector>
  mop transpose <matrix>
  mop show <matrix>
  mop (-h | --help)
  mop --version

Options:

  --max-iterations=NUM  Set the max number of iterations for gmres [default: 100000]
  --tolerance=NUM       Set the tolerance for gmres [default: 0.0000001]
  --max-directions=NUM  Set the max search directions for gmres [default: 50]
  --lhs-test            Use the given vector as x instead of the solution
  -h --help             Show this screen.
  -v --vec              Multiply by a vector instead of a matrix.
  --version             Show version.
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_vec: bool,
    flag_lhs_test: bool,
    flag_tolerance: f64,
    flag_max_directions: usize,
    flag_max_iterations: usize,
    arg_matrix: Option<String>,
    arg_matrix2: Option<String>,
    arg_vector: Option<String>,
    cmd_gmres: bool,
    cmd_mul: bool,
    cmd_transpose: bool,
    cmd_show: bool,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    let vec_parser = matrix::VectorParser::new();
    let lhs = from_file(Path::new(&args.arg_matrix.expect("Please enter a matrix path"))).expect("ERROR: Could not read matrix");
    //Handle GMRES
    if args.cmd_gmres {
        let other_vec = vec_parser.parse(&args.arg_vector.expect("Please enter a vector")).expect("Failed to parse vector");
        let result = if args.flag_lhs_test {
            lhs.gmres(
                lhs.vec_mul(&other_vec).expect("Failed to multiply by vector"),
                args.flag_max_iterations,
                args.flag_tolerance,
                args.flag_max_directions
            )
        } else {
            lhs.gmres(
                other_vec,
                args.flag_max_iterations,
                args.flag_tolerance,
                args.flag_max_directions
            )
        };
        match result {
            Ok(v) => println!("GMRES Successful: {:?}",v),
            Err(e) => println!("GMRES Failed: {:?}",e)
        }
    //Handle multiplication
    } else if args.cmd_mul {

        if args.flag_vec {
            let other_vec = vec_parser.parse(&args.arg_vector.expect("Please enter a vector")).expect("Failed to parse vector");
            let result = lhs.vec_mul(&other_vec);
            match result {
                Ok(v) => println!("{:?}",v),
                Err(e) => println!("Multiplication failed: {:?}",e)
            }

        } else {
            let rhs = from_file(Path::new(&args.arg_matrix2.expect("Please enter a matrix path"))).expect("ERROR: Could not read matrix");
            let result = lhs.safe_mul(&rhs);
            match result {
                Ok(v) => println!("{}",v),
                Err(e) => println!("Multiplication failed: {:?}",e)
            }

        }
    //Handle transpose
    } else if args.cmd_transpose {
        let transpose = lhs.transpose();
        println!("{}",transpose);
    //Show a matrix without performing any operation
    } else if args.cmd_show {
        println!("{}",lhs);
    }
}
